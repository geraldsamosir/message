const express = require('express');
const app = express();
const Port = 3000;
const firebase = require('./router/firebase');
const nexmo = require('./router/nexmo');
const sendgrid = require('./router/sendgrid');
const enrouten = require('express-enrouten')

app.use('/', enrouten({
  directory: './router'
}));


app.listen(Port, ()=>{
  console.log(`server run in port ${Port}`)
});