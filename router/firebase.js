const firebase = require("firebase-admin");
const serviceAccount = require("../config/firebase.json");

firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount),
  databaseURL: "https://newagent-9d822.firebaseio.com"
});

const notification_options = {
  //message priority
  priority: "high",
  // notification live time
  timeToLive: 60 * 60 * 24
};

class Firebase {
  constructor(router){
    this.index = this.singleNotification.bind(this);
    //  /firebase/?registration-token=registrationtoken
    router.get('/', this.singleNotification)
  }

  //handle single notification
  async singleNotification(){
    const  registrationToken = req.query.registrationToken
    const message = 'hello message from firebase'
    const options =  notification_options;
    
    try {
        admin.messaging().sendToDevice(registrationToken, message, options)
        return res.status(200).json({
            message: 'success send firebase notification'
        })
    } catch (error) {
        return res.status(500).json({
            message: 'failed send notification'
        })
    }
  }

  // handle multiple notification

};

module.exports = (router)=> new Firebase(router);