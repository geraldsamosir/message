const NexmoClient = require('nexmo');
const rp = require('request-promise');
const nexmo = {
  apiKey: '25abc7f6',
  apiSecret: 'dIxhA6sXg8XrOEjr',
};

class Nexmo {
    constructor(router){
      this.index = this.index.bind(this);
      this.whatsapp = this.whatsapp.bind(this);
      router.get('/', this.index)
      router.get('/whatsapp', this.whatsapp)
    }
    // sms
    async index(req,res) {
      const from = 'Vonage APIs';
      const to = '62895383065929';
      const text = 'Hello from Vonage SMS API update';
      try {
        //nexmo.message.sendSms(from, to, text);
        const response = await rp({
            uri: 'https://rest.nexmo.com/sms/json',
            method: 'POST',
            qs: {
                from: 'Vonage APIs',
                text,
                to,
                api_key: nexmo.apiKey,
                api_secret: nexmo.apiSecret
            },
            json: true
        })
        res.json(response)   
      } catch (error) {
         res.json({
            message: 'nexmo send sms fail'
         }) 
      }
    }
    // whatsapp
    async whatsapp(req, res) {
      try {
        const targetmesssage = '6282168207251';
        const nexmoSanboxAcount = '14157386170';
        const basicAuth = {
            username: '25abc7f6',
            password: 'dIxhA6sXg8XrOEjr'
        }
        const response = await rp( {
          uri: 'https://messages-sandbox.nexmo.com/v0.1/messages',
          method: 'POST',
          auth: basicAuth,
          body: {
            "from": { "type": "whatsapp", "number": nexmoSanboxAcount },
            "to": { "type": "whatsapp", "number": targetmesssage },
            "message": {
              "content": {
                "type": "text",
                "text": "This is a WhatsApp Message sent from the Messages API"
              }
            }},
          json: true
      })
      console.log('sinininini', response);
      res.status(200).json(response);    
      } catch (error) {
        res.status(500).json({
            message: 'internal server error'
        })
      }
    }
}   

module.exports = (router)=> new Nexmo(router);