class Sendgrid {
    constructor(router){
      this.index = this.index.bind(this);
      router.get('/', this.index);
    }
    async index(req,res) {
      res.json({
        message: 'ok got it'
      })
    }
};

module.exports = (router)=> new Sendgrid(router);
